//
//  Media.swift
//  Registration
//
//  Created by Sunilkumar Basappa on 09/06/17.
//  Copyright © 2017 Softvision. All rights reserved.
//

import UIKit

protocol MediaDelegate {
     func getProfilePicture(_ image : UIImage)
}

struct MediaOptions {
    
    static let camera   = "Camera"
    static let gallery  = "Gallery"
    static let cancel   = "Cancel"
}

private var mediaInstance = Media()

class Media: NSObject {

    let title = "Media"
    let message = "Please select your options"
    
    var viewController : UIViewController!
    var deleagte : MediaDelegate? = nil
    
    class var sharedMedia : Media {
        return mediaInstance
    }
    
    func alertViewController(_ vctr : UIViewController,_ delegate : MediaDelegate)
    {
        self.deleagte = delegate
        self.viewController = vctr
        let alertView = UIAlertView()
        alertView.delegate  = self
        alertView.title = title
        alertView.message = message
        alertView.addButton(withTitle: MediaOptions.camera)
        alertView.addButton(withTitle: MediaOptions.gallery)
        alertView.addButton(withTitle: MediaOptions.cancel)
        alertView.show()
    }
    
    func camera()
    {
        setMediaPickerControllerWithSourceType(.camera)
    }
    
    func gallery()
    {
        setMediaPickerControllerWithSourceType(.photoLibrary)
    }

    func setMediaPickerControllerWithSourceType(_ sourceType : UIImagePickerControllerSourceType)
    {
        let imagePickerCtr = UIImagePickerController()
        imagePickerCtr.sourceType = sourceType
        imagePickerCtr.delegate = self
        imagePickerCtr.allowsEditing  = true
        self.viewController.present(imagePickerCtr, animated: true, completion: nil)
    }
}


extension Media : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info["UIImagePickerControllerOriginalImage"]
       if deleagte != nil
       {
            deleagte?.getProfilePicture(image as! UIImage)
        }
        self.viewController.dismiss(animated: true, completion: nil)
    }
}

extension Media : UIAlertViewDelegate
{
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if alertView.buttonTitle(at: buttonIndex) == MediaOptions.camera
        {
            camera()
            
        }else if alertView.buttonTitle(at: buttonIndex) == MediaOptions.gallery
        {
            gallery()
        }
    }
}





