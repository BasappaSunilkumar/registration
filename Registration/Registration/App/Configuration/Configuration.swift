//
//  Configuration.swift
//  Registration
//
//  Created by Sunilkumar Basappa on 09/06/17.
//  Copyright © 2017 Softvision. All rights reserved.
//

import UIKit

private var configurationInstance  =  Configuration()

class Configuration: NSObject {

    var infoDictionary = [String:Any]()

    //Configuration instance
    class var sharedConfiguration : Configuration {
        return configurationInstance
    }
    
    override init() {
        super.init()
        self.infoDictionary = Bundle.main.infoDictionary!
    }
    
    //App Name
    func appName() -> String {
        return self.infoDictionary["AppName"] as! String
    }
    
    //light font
    func fontWithSize(size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size)
    }
    
    //URL
    func serviceURL()-> String
    {
        return self.infoDictionary["ServiceURL"] as! String
    }
    
}
