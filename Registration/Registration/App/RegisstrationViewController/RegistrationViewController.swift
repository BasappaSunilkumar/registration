//
//  RegistrationViewController.swift
//  Registration
//
//  Created by Sunilkumar Basappa on 09/06/17.
//  Copyright © 2017 Softvision. All rights reserved.
//

import UIKit

struct  RegisterTableAttribute {
    
    static let numberOfRows         = 8
    static let cellIdentifier       = "RegisterCell"
}

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profilePicture: UIImageView!
    
    var registrationParmas : [String : AnyObject] = [:]
    var masterData : [String :  AnyObject] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        getMasterData()
        self.reloadTableView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.navigationBar.isHidden = true
    }

    func reloadTableView()
    {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        LocationManger.sharedLocationManger.getCurrentLocation(self)
    }

    @IBAction func profileBtnTapped(_ sender: Any) {
        
        Media.sharedMedia.alertViewController(self, self)
        
    }
    
    func getMasterData()
    {
        RegistrationAPI.sharedRegistrationAPI.getMasterData(completionHandler: { (data,error) -> Void in
            
            if data != nil
            {
                do
                {
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as! [String : AnyObject]
                    DispatchQueue.main.async(execute: {
                        self.masterData = response["MasterData"] as! [String : AnyObject]
                       
                        
                    })
                    print("")
                }catch
                {
                    
                }
            }
            
        })
    }
    
    @IBAction func registerBtnTapped(_ sender: Any) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RegistrationViewController : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RegisterTableAttribute.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: RegisterTableAttribute.cellIdentifier) as? RegistrationTableViewCell
        
        if (cell == nil)
        {
            cell = RegistrationTableViewCell(style: .default, reuseIdentifier: RegisterTableAttribute.cellIdentifier)
        }
        cell?.selectionStyle = .none
        cell?.registrationView(indexPath, self, registrationParmas)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.row == 5)
        {
            let jobTypeVctr = JobTypeViewController(self.masterData["JobTypes"] as! [AnyObject], self)
            self.navigationController?.pushViewController(jobTypeVctr, animated: true)
        }else  if (indexPath.row == 7) {
            let educationalQualificationVctr = EducationalQualificationViewController(educationQualifications: self.masterData["EducationalQualification"] as! [AnyObject],delegate : self)
            self.navigationController?.pushViewController(educationalQualificationVctr, animated: true)
        }

    }
}

extension RegistrationViewController : MediaDelegate
{
    func getProfilePicture(_ image : UIImage)
    {
        let image = image
        self.profilePicture.image = image
        let imageData = UIImagePNGRepresentation(image)
        let dataImage = imageData?.base64EncodedString(options: .lineLength64Characters)
        registrationParmas["photoBitmap"] = dataImage as AnyObject
    }
}

extension RegistrationViewController : RegistrationProtocol
{
    func appendingRegistrationParams(_ textFiled : UITextField)
    {
        switch textFiled.tag {
        case 0:
            registrationParmas["firstname"] = textFiled.text as AnyObject
        case 1:
            registrationParmas["lastname"] = textFiled.text as AnyObject
        case 2:
            registrationParmas["gender"] = textFiled.text as AnyObject
        case 3:
            registrationParmas["phoneNumber"] = textFiled.text as AnyObject
        case 4:
            registrationParmas["emailId"] = textFiled.text as AnyObject
        case 5:
            registrationParmas["jobType"] = textFiled.text as AnyObject
        case 6:
            registrationParmas["address"] = textFiled.text as AnyObject
        case 7:
            registrationParmas["educaitonalQualification"] = textFiled.text as AnyObject
        default:
            print("")
        }
    }
}

extension RegistrationViewController  :LocationMangerDelegate
{
    func currentLocationWithAddress(_ address : String , _ latitude : Double, _ longitude : Double)
    {
        registrationParmas["latitude"] = latitude as AnyObject
        registrationParmas["longitude"] = longitude as AnyObject
        registrationParmas["address"] = address as AnyObject
        self.tableView.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .none)
    }
}

extension RegistrationViewController : JobTypeDeleagte
{
    func getJobType(_ jobType : String)
    {
         registrationParmas["jobType"] = jobType as AnyObject
         //self.tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .none)
    }
}
extension RegistrationViewController: Education {
    
    func educationcalQualifiactions(_ qualifications:[String]) {
       
    }
    
}
