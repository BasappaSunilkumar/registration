//
//  JobTypeViewController.swift
//  Registration
//
//  Created by Sunilkumar Basappa on 09/06/17.
//  Copyright © 2017 Softvision. All rights reserved.
//

import UIKit

protocol JobTypeDeleagte {
     func getJobType(_ jobType : String)
}

struct  JobTypeTableAttribute {

    static let cellIdentifier       = "RegisterCell"
}


class JobTypeViewController: UIViewController {
    
    
    var jobType : [AnyObject] = []
    var delegate : JobTypeDeleagte? = nil
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableView: UITableView!
    
    var filterJobType : [String] = []
    var jobData : [String] = []
    
    convenience init(_ jobType : [AnyObject], _ delegate : JobTypeDeleagte)
    {
        self.init()
        self.jobType = jobType
        self.delegate = delegate
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Job Type"
        for item in self.jobType
        {
            let data = item as! [String :  String]
            self.jobData.append(data["JobType"]!)
        }
        self.filterJobType = self.jobData
        self.reloadTableView()
        // Do any additional setup after loading the view.
    }
    
    func reloadTableView()
    {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }*/
}

extension JobTypeViewController : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterJobType.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: JobTypeTableAttribute.cellIdentifier)
        if (cell == nil)
        {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: JobTypeTableAttribute.cellIdentifier)
        }
        cell?.textLabel?.text = self.filterJobType[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil
        {
            delegate?.getJobType(self.filterJobType[indexPath.row])
        }
        self.navigationController?.popViewController(animated: true)
    }
}


extension JobTypeViewController : UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterJobType = searchText.isEmpty ? self.jobData : self.jobData.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        self.reloadTableView()
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.filterJobType = (searchBar.text?.isEmpty)! ? self.jobData : self.jobData.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: searchBar.text!, options: .caseInsensitive) != nil
        })
        
        self.reloadTableView()

    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
}
