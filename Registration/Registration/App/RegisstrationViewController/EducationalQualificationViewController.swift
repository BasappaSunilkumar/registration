//
//  EducationalQualificationViewController.swift
//  Registration
//
//  Created by Sunilkumar Basappa on 09/06/17.
//  Copyright © 2017 Softvision. All rights reserved.
//

import UIKit

protocol Education {
    func educationcalQualifiactions(_ qualifications:[String])
}

class EducationalQualificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    static let cellIdentifier       = "EducationCell"
    var educationQualifications = [AnyObject]()
    var selectedQualifications = [String]()
    var delegate : Education? = nil

    convenience init(educationQualifications: [AnyObject],delegate : Education) {
        self.init()
        self.educationQualifications = educationQualifications
        self.delegate = delegate
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Qualification"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target:self, action:  #selector(doneButtonTapped))
        
        let tableview = UITableView(frame: self.view.frame)
        self.view.addSubview(tableview)
        tableview.tableFooterView = UIView()
        tableview.delegate = self
        tableview.dataSource = self
        tableview.reloadData()
    }
    
    func doneButtonTapped() {
        if (self.delegate != nil) {
            let qualifications = NSSet(array: self.selectedQualifications)
            self.delegate?.educationcalQualifiactions(qualifications.allObjects as! [String])
        }
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.educationQualifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: EducationalQualificationViewController.cellIdentifier)
        if (cell == nil)
        {
            cell = UITableViewCell(style: .default, reuseIdentifier: EducationalQualificationViewController.cellIdentifier)
        }

        cell?.textLabel?.text = (educationQualifications[indexPath.row] as! [String:String])["Qualification"]!
        cell?.selectionStyle = .none

        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        if (cell?.accessoryType == UITableViewCellAccessoryType.checkmark) {
            
            cell?.accessoryType = UITableViewCellAccessoryType.none
            
        }else {
            cell?.accessoryType = UITableViewCellAccessoryType.checkmark
            self.selectedQualifications.append((cell?.textLabel?.text)!)

        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

