//
//  RegistrationAPI.swift
//  Registration
//
//  Created by Sunilkumar Basappa on 09/06/17.
//  Copyright © 2017 Softvision. All rights reserved.
//

import UIKit

struct APIOptions {
    static let post        :  String  = "POST"
    static let get         :  String  = "Get"
    static let contentType  :  String  = "application/json; charset=utf-8"
    static let contentLength : String = "Content-Length"
}


private var utilityRegistrationAPI = RegistrationAPI()

class RegistrationAPI: NSObject {

    class var sharedRegistrationAPI : RegistrationAPI {
        return utilityRegistrationAPI
    }
    
    //Policy Details
    func getMasterData(completionHandler : @escaping(Data?,Error?)-> ())
    {
        var request = URLRequest(url: URL(string : "\(Configuration.sharedConfiguration.serviceURL())getMasterData")!, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 120.0)
        request.httpMethod = APIOptions.get
        request.addValue(APIOptions.contentType, forHTTPHeaderField: "Content-Type")
        
        let sestion = URLSession.shared
        let sestionTask = sestion.dataTask(with: request, completionHandler: {(data,response,error) -> Void in
            if data != nil
            {
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        completionHandler(nil, error)
                        
                    }
                    else {
                        completionHandler(data, error)
                    }
                }
            }
        })
        sestionTask.resume()
        
    }

    
}
