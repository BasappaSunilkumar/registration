//
//  LocationManger.swift
//  Registration
//
//  Created by Sunilkumar Basappa on 09/06/17.
//  Copyright © 2017 Softvision. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationMangerDelegate {
     func currentLocationWithAddress(_ address : String , _ latitude : Double, _ longitude : Double)
}

private var LocationMangerInstance = LocationManger()

class LocationManger: NSObject {

    var locationManger = CLLocationManager()
    var delagte : LocationMangerDelegate? = nil
    var didUpdateLocation : Bool = false
    
    class var sharedLocationManger : LocationManger {
        return LocationMangerInstance
    }
    
    func getCurrentLocation(_ deleagte : LocationMangerDelegate)
    {
        self.delagte = deleagte
        locationManger = CLLocationManager()
        locationManger.requestWhenInUseAuthorization()
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.distanceFilter = kCLDistanceFilterNone
        locationManger.startUpdatingLocation()
    }
    
}

extension LocationManger : CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if !didUpdateLocation
        {
            didUpdateLocation = true
            let location = locations.last
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(location!, completionHandler: { (placemarks,error) -> Void in
                
                let placeMark : CLPlacemark!
                placeMark = placemarks?[0]
                let addressDictionary = placeMark.addressDictionary
                 let formattedAddress = addressDictionary?["FormattedAddressLines"]! as! [AnyObject]
                var addess : String = ""
                for item in formattedAddress
                {
                    addess.append("\(item )")
                }
                
                if self.delagte != nil
                {
                    self.delagte?.currentLocationWithAddress(addess, (location?.coordinate.latitude)!, (location?.coordinate.longitude)!)
                }
            })
        }
    }
}
