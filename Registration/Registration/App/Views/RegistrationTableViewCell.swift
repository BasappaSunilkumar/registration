//
//  RegistrationTableViewCell.swift
//  Registration
//
//  Created by Sunilkumar Basappa on 09/06/17.
//  Copyright © 2017 Softvision. All rights reserved.
//

import UIKit

protocol RegistrationProtocol {
     func appendingRegistrationParams(_ textFiled : UITextField)
}

enum Registration : Int
{
    case FirstName = 0
    case LastName
    case Gender
    case PhoneNumber
    case EmailAddress
    case JobTypeInterestesIn
    case Address
    case EducationQualification
}


class RegistrationTableViewCell: UITableViewCell {

    var deleagte : RegistrationProtocol? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func registrationView(_ indexPath : IndexPath, _ deleagte : RegistrationProtocol, _ registrationParmas : [String : AnyObject])
    {
        self.deleagte = deleagte
        let frame = self.frame
        //var xPos ,yPos,width,height : CGFloat = 0.0
        let view = UIView(frame:frame)
        
        let padding : CGFloat = 10.0
        //Adding textfield
        view.addSubview(createTextFiledWithFrame((CGRect(x: padding, y: 0.0, width: (frame.size.width - (2 * padding )), height: frame.size.height)), indexPath, registrationParmas))
        self.contentView.addSubview(view)
    }
    
    func createTextFiledWithFrame(_ frame : CGRect, _ indexPath : IndexPath, _ params : [String : AnyObject]) -> UITextField
    {
        let textFiled = UITextField(frame: frame)
         textFiled.placeholder = returnPlaceHolderWithTag(Registration(rawValue: indexPath.row)!)
        textFiled.tag = indexPath.row
        textFiled.keyboardType = returnKeyBoardTypeWithTag(Registration(rawValue: indexPath.row)!)
        textFiled.text = setValueWithTag(Registration(rawValue: indexPath.row)!, params)
        textFiled.textColor = UIColor.black
        textFiled.delegate = self
        textFiled.font = Configuration.sharedConfiguration.fontWithSize(size: 14.0)
        let placeHolderlbl = UILabel(frame: CGRect(x: 0.0, y: 10.0, width: 100.0, height: 20.0))
        placeHolderlbl.font = Configuration.sharedConfiguration.fontWithSize(size: 12.0)
        placeHolderlbl.textColor = UIColor.black
        placeHolderlbl.isHidden = true
        placeHolderlbl.text = returnPlaceHolderWithTag(Registration(rawValue: indexPath.row)!)
        textFiled.addSubview(placeHolderlbl)
        textFiled.isEnabled = (indexPath.row == 5 || indexPath.row == 7) ? false : true
        return textFiled
    }
    
    func setValueWithTag(_ tag : Registration ,_ params : [String : AnyObject]) -> String?
    {
        switch tag {
        case .FirstName:
            return params["firstname"] as? String
        case .LastName:
            return params["lastname"] as? String
        case .Gender:
            return params["gender"] as? String
        case .PhoneNumber:
            return params["phoneNumber"] as? String
        case .EmailAddress:
            return params["emailId"] as? String
        case .JobTypeInterestesIn:
            return params["jobType"] as? String
        case .Address:
            return params["address"] as? String
        case .EducationQualification:
            return params["educaitonalQualification"] as? String
        }

    }
    
    func returnKeyBoardTypeWithTag(_ tag : Registration) -> UIKeyboardType
    {
        switch tag {
        case .EmailAddress:
            return UIKeyboardType.emailAddress
        case .PhoneNumber:
            return UIKeyboardType.numberPad
        default:
            return UIKeyboardType.default
        }
    }
    
    func returnPlaceHolderWithTag(_ tag : Registration)-> String
    {
        switch tag {
        case .FirstName:
            return "First Name"
        case .LastName:
            return "Last Name"
        case .Gender:
            return "Gender"
        case .PhoneNumber:
            return "Phone Number"
        case .EmailAddress:
            return "Email address"
        case .JobTypeInterestesIn:
            return "Job Type interested in"
        case .Address:
            return "Address"
        case .EducationQualification:
            return "Education qualification"
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension RegistrationTableViewCell : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {

        KeyBoard.shareKeyboard.keyboardAnimationWithTextField(textField: textField, view:  (self.superview?.superview?.superview)!, isMove: true)
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.characters.count > 0
        {
            Utility.sharedUtility.labelAnimation(textfield: textField, isHidden: false)
            
        }else if (textField.text?.characters.count)! <= 1 || (string == "" && range.location == 0)
        {
            Utility.sharedUtility.labelAnimation(textfield: textField, isHidden: true)
        }

        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        KeyBoard.shareKeyboard.keyboardAnimationWithTextField(textField: textField, view:  (self.superview?.superview?.superview)!, isMove: false)
        
        if deleagte != nil
        {
            deleagte?.appendingRegistrationParams(textField)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
