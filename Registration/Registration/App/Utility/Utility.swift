//
//  Utility.swift
//  Registration
//
//  Created by Sunilkumar Basappa on 09/06/17.
//  Copyright © 2017 Softvision. All rights reserved.
//

import UIKit

private var utilityInstance = Utility()

class Utility: NSObject {

    class var sharedUtility : Utility {
        return utilityInstance
    }

    //show label animation
    func labelAnimation(textfield: UITextField, isHidden: Bool) {
        let subviews = textfield.subviews
        let placeholderLbl = subviews[0]
        UIView.animate(withDuration: 0.4, animations: {
            placeholderLbl.isHidden = isHidden
            placeholderLbl.frame = CGRect(x: (placeholderLbl.frame.origin.x), y: (isHidden) ? 20.0 : -5.0, width: (placeholderLbl.frame.size.width), height: (placeholderLbl.frame.size.height))
            print("")
        }, completion: { finished in
            
        })
        
    }
    
}

